package SourceClasses;

/**
 * Created with IntelliJ IDEA.
 * User: Klimov Denis
 * Date: 20.05.14
 * Time: 21:46
 * To change this template use File | Settings | File Templates.
 */
public class ListWorking {
    private int id;
    private String name;
    private int time;

    public ListWorking(int _id, String _name, int _time) {
        this.setId(_id);
        this.setName(_name);
        this.setTime(_time);
    }

    private void setName(String _name) {
        this.name = _name;
    }

    private void setTime(int _time) {
        this.time = _time;
    }

    public String getName() {
        return name;
    }

    public int getTime() {
        return time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
