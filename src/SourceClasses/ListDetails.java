package SourceClasses;

/**
 * Created with IntelliJ IDEA.
 * User: Klimov Denis
 * Date: 20.05.14
 * Time: 21:41
 * To change this template use File | Settings | File Templates.
 */
public class ListDetails {
    private int id;
    private int pid;
    private int count;
    private int price;
    private int needToDeal;
    private int makingProcess;

    private String name;

    public ListDetails(int _id, int _pid, String _name,
                       int _count, int _price, int _needToDeal,
                       int _makingProcess) {
        this.setId(_id);
        this.setPid(_pid);
        this.setCount(_count);
        this.setPrice(_price);
        this.setNeedToDeal(_needToDeal);
        this.setMakingProcess(_makingProcess);
        this.setName(_name);
    }

    private void setId(int _id) {
        this.id = _id;
    }

    private void setPid(int _pid) {
        this.pid = _pid;
    }

    private void setName(String _name) {
        this.name = _name;
    }

    public int getId() {
        return id;
    }

    public int getPid() {
        return pid;
    }

    public String getName() {
        return name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getNeedToDeal() {
        return needToDeal;
    }

    public void setNeedToDeal(int needToDeal) {
        this.needToDeal = needToDeal;
    }

    public int getMakingProcess() {
        return makingProcess;
    }

    public void setMakingProcess(int makingProcess) {
        this.makingProcess = makingProcess;
    }
}
