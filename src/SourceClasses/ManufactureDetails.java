package SourceClasses;

/**
 * Created with IntelliJ IDEA.
 * User: Klimov Denis
 * Date: 16.05.14
 * Time: 2:10
 * To change this template use File | Settings | File Templates.
 */
public class ManufactureDetails {
    private String name;
    private String type;

    public ManufactureDetails(String _name, String _type) {
        this.setName(_name);
        this.setType(_type);
    }

    private void setName(String _name) {
        this.name = _name;
    }

    public String getName() {
        return name;
    }

    private void setType(String _type) {
        this.type = _type;
    }

    public String getType() {
        return type;
    }
}
