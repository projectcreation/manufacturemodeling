package SourceClasses;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: Klimov Denis
 * Date: 21.05.14
 * Time: 12:01
 * To change this template use File | Settings | File Templates.
 */
public class DealsFeatures implements Serializable {
    private String detailsName;

    private boolean isOrder;

    private int count;
    private int morePrice;
    private int price;
    private int moreTime;
    private int time;

    public DealsFeatures(String _detailsName, boolean _isOrder, int _count,
                         int _price, int _morePrice, int _time, int _moreTime) {
        this.setDetailsName(_detailsName);
        this.setOrder(_isOrder);
        this.setCount(_count);
        this.setPrice(_price);
        this.setMorePrice(_morePrice);
        this.setTime(_time);
        this.setMoreTime(_moreTime);
    }

    private void setDetailsName(String _detailsName) {
        this.detailsName = _detailsName;
    }

    private void setOrder(boolean _isOrder) {
        this.isOrder = _isOrder;
    }

    private void setCount(int _count) {
        this.count = _count;
    }

    private void setPrice(int _price) {
        this.price = _price;
    }

    private void setTime(int _time) {
        this.time = _time;
    }

    private void setMorePrice(int _morePrice) {
        this.morePrice = _morePrice;
    }

    private void setMoreTime(int _moreTime) {
        this.moreTime = _moreTime;
    }

    public String getDetailsName() {
        return detailsName;
    }

    public boolean getOrder() {
        return isOrder;
    }

    public int getCount() {
        return count;
    }

    public int getPrice() {
        return price;
    }

    public int getTime() {
        return time;
    }

    public int getMorePrice() {
        return morePrice;
    }

    public int getMoreTime() {
        return moreTime;
    }
}
