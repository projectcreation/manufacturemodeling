package SourceClasses;

import jade.core.AID;

/**
 * Created with IntelliJ IDEA.
 * User: Klimov Denis
 * Date: 14.05.14
 * Time: 20:10
 * To change this template use File | Settings | File Templates.
 */
public class Department {
    private String name;
    private AID address;

    public Department(String _name, AID _address) {
        this.setName(_name);
        this.setAdress(_address);
    }

    private void setName(String name) {
        this.name = name;
    }

    private void setAdress(AID _address) {
        this.address = _address;
    }

    public String getName() {
        return name;
    }

    public AID getAddress() {
        return address;
    }
}
