package SourceClasses;

/**
 * Created with IntelliJ IDEA.
 * User: Klimov Denis
 * Date: 20.05.14
 * Time: 21:24
 * To change this template use File | Settings | File Templates.
 */
public class Working {
    private String name;
    private int time;

    public Working(String _name, int _time) {
        this.setName(_name);
        this.setTime(_time);
    }

    private void setName(String _name) {
        this.name = _name;
    }

    private void setTime(int _time) {
        this.time = _time;
    }

    public String getName() {
        return name;
    }

    public int getTime() {
        return time;
    }
}
