package SourceClasses;

import java.io.Serializable;
import java.util.ArrayList;

public class Details implements Serializable {
    private String Case;
    private String CPU;
    private String RAM;
    private String Display;

    private int Count;

    private ArrayList<String> _devicesList = new ArrayList<String>();

    public Details(String _case, String _cpu,
                   String _ram, String _display,
                   int _count) {
        this.setCase(_case);
        this.setCPU(_cpu);
        this.setRAM(_ram);
        this.setDisplay(_display);
        this.setCount(_count);
        this.setDevicesList();
    }

    private void setCount(int _count) {
        this.Count = _count;
    }

    private void setCase(String _case) {
        this.Case = _case;
    }

    private void setCPU(String _cpu) {
        this.CPU = _cpu;
    }

    private void setRAM(String _ram) {
        this.RAM = _ram;
    }

    private void setDisplay(String _display) {
        this.Display = _display;
    }

    private void setDevicesList(){
        this._devicesList.add(Case);
        this._devicesList.add(CPU);
        this._devicesList.add(RAM);
        this._devicesList.add(Display);
    }

    public int getCount() {
        return Count;
    }

    public String getCase() {
        return Case;
    }

    public String getCPU() {
        return CPU;
    }

    public String getRAM() {
        return RAM;
    }

    public String getDisplay() {
        return Display;
    }

    public ArrayList<String> getDevicesList() {
        return _devicesList;
    }
}