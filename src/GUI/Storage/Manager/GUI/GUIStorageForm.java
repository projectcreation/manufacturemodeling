package GUI.Storage.Manager.GUI;

import DBAgents.DBAgent;
import GUI.About.GUI.*;
import GUI.Setting.GUI.DBGUIForm;
import SourceClasses.ListDetails;
import jade.gui.GuiEvent;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/*Реализация класса, расширяещего возомжности стандартного класса DefaultTreeModel*/
class DetailsRelationships extends DefaultTreeModel {
    private HashMap<String, DefaultMutableTreeNode> namesToNodes = new HashMap<String, DefaultMutableTreeNode>();

    public DetailsRelationships(DefaultMutableTreeNode root, ArrayList<ListDetails> _detailsRelationship) {
        super(root);
        namesToNodes.put("root", root);
        createRelationships(_detailsRelationship);
    }

    public void createRelationships(ArrayList<ListDetails> _array) {
        for (ListDetails aList : _array) {
            if(aList.getPid() == -1) {
                this.addNodeTo(aList.getName(), "root");
            }
            else {
                for (ListDetails bList : _array) {
                    if (aList.getPid() == bList.getId()) {
                        this.addNodeTo(aList.getName(), bList.getName());
                    }
                }
            }
        }
    }

    public void addNodeTo(String childNodeName, String parentNodeName) {

        DefaultMutableTreeNode parentNode = getNode(parentNodeName);
        DefaultMutableTreeNode childNode = getNode(childNodeName);

        insertNodeInto(childNode, parentNode, parentNode.getChildCount());
    }

    private DefaultMutableTreeNode getNode(String name) {
        name = name.toLowerCase();

        DefaultMutableTreeNode node = (namesToNodes.get(name));

        if(node == null) {
            DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(name);

            namesToNodes.put(name, newNode);

            return newNode;
        }
        else {
            return node;
        }
    }
}
/*================================================================================*/

public class GUIStorageForm extends JFrame {
    private DetailsRelationships _myTreeModel;
    private DBAgent storageManager;
    private String fileName;

    private JPanel rootPanel;

    private JTree treeDetails;

    private JTextArea messageDetail;
    private JTextArea logMessage;

    private JScrollPane treeScrollPane;
    private JScrollPane messageDetailScrollPane;
    private JScrollPane logMessageScrollPane;

    public boolean isConnected = false;

    /*Создание меню и полная его реализация*/
    private void CreateMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");

        JMenuItem connectMenu = new JMenuItem("Connect");
        fileMenu.add(connectMenu);
        connectMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK));

        JMenuItem disconnectMenu = new JMenuItem("Disconnect");
        fileMenu.add(disconnectMenu);
        disconnectMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK));

        fileMenu.addSeparator();

        JMenuItem settingsMenu = new JMenuItem("Settings");
        fileMenu.add(settingsMenu);

        fileMenu.addSeparator();

        JMenuItem exitMenu = new JMenuItem("Exit");
        fileMenu.add(exitMenu);

        JMenu helpMenu = new JMenu("Help");

        JMenuItem aboutMenu = new JMenuItem("About");
        helpMenu.add(aboutMenu);

        menuBar.add(fileMenu);
        menuBar.add(helpMenu);

        connectMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ConnectAgentToDF();
            }
        });

        disconnectMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DisconnectAgentFromDF();
            }
        });

        settingsMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DBGUIForm settingForm = new DBGUIForm(GUIStorageForm.this, fileName);
            }
        });

        exitMenu.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        aboutMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AboutGUIForm aboutForm = new AboutGUIForm("Storage Manager", GUIStorageForm.this);
            }
        });

        this.setJMenuBar(menuBar);
    }
    /*=====================================*/

    /*Реализация взаимодействия с системой DF через GUIAgent*/
    private void ConnectAgentToDF() {
        if(!isConnected) {
            GuiEvent _guiEvent = new GuiEvent(this, 0);
            storageManager.postGuiEvent(_guiEvent);

            isConnected = true;
        }
        else {
            printLogMessage("You already registered in the system!");
        }
    }

    private void DisconnectAgentFromDF() {
        if(isConnected) {
            GuiEvent _guiEvent = new GuiEvent(this, 1);
            storageManager.postGuiEvent(_guiEvent);

            isConnected = false;
        }
        else {
            printLogMessage("You are not registering in the system!");
        }
    }
    /*======================================================*/

    /*Функция для вывода сообщений либо от GUIAgent, либо от самого класса, реализующий
    * графический интерфейс*/
    public void printLogMessage(String _message) {
        Date dataTime = new Date();
        SimpleDateFormat formatDataTime = new SimpleDateFormat("[HH:mm:ss]: ");

        logMessage.append(formatDataTime.format(dataTime) + _message + "\n");
        logMessage.setCaretPosition(logMessage.getDocument().getLength());
    }
    /*================================================================================*/

    public void printInformationAboutDetail(String _detailsName) {
        messageDetail.setText(null);

        messageDetail.setLineWrap(true);
        messageDetail.setFont(new Font("Times New Roman", 0, 16));


        ListDetails _gottenDetail = storageManager.getDetailByName(_detailsName);
        if(_gottenDetail.getPid()==-1) {
            if(_gottenDetail.getName().equalsIgnoreCase("display")) {
                messageDetail.append("You can find information about different displays and their details.");
            }
            if(_gottenDetail.getName().equalsIgnoreCase("cpu")) {
                messageDetail.append("You can find information about different CPU's and their details.");
            }
            if(_gottenDetail.getName().equalsIgnoreCase("ram")) {
                messageDetail.append("You can find information about different RAM's and their details.");
            }
            if(_gottenDetail.getName().equalsIgnoreCase("case")) {
                messageDetail.append("You can find information about different cases and their details.");
            }
        }
        else {
            messageDetail.append("Count: " + _gottenDetail.getCount() + "\n" +
                             "Price: " + _gottenDetail.getPrice() + "\n" +
                             "Making process: " + storageManager.getWorkingById(_gottenDetail.getMakingProcess()) + "\n" +
                             "Need to make: " + _gottenDetail.getNeedToDeal() + "\n");
        }
    }

    /*Функция заполнения модели данных в jTree данными из БД*/
    public void createTreeModel(ArrayList<ListDetails> _list) {
        _myTreeModel = new DetailsRelationships(new DefaultMutableTreeNode("Device"),
                _list);
        treeDetails.setModel(_myTreeModel);
    }

    private void settingTree() {
        treeDetails.setModel(null);

        treeDetails.addTreeSelectionListener(new TreeSelectionListener() {
            public void valueChanged(TreeSelectionEvent e) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.getPath().getLastPathComponent();
                printInformationAboutDetail(node.toString());
            }
        });
    }
    /*======================================================*/

    public GUIStorageForm(DBAgent _e, String _fileName) {
        super("Storage Manager");

        fileName = _fileName;
        storageManager = _e;

        setContentPane(rootPanel);
        CreateMenuBar();

        setResizable(false);
        setSize(640,565);
        setLocationRelativeTo(null);

        settingTree();

        setVisible(true);

        printLogMessage("Welcome to Storage Manager!");
    }
}
