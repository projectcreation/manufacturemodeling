package GUI.Manufctures.Manager.GUI;

import GUI.About.GUI.*;
import Manufactures.GUIManufacture;
import jade.gui.GuiEvent;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GUIManufactureForm extends JFrame {
    private GUIManufacture manufactureManager;

    private JPanel rootPanel;
    private JTextArea logMessage;

    private boolean isConnected = false;

    /*Создание меню и полная его реализация*/
    private void CreateMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");

        JMenuItem connectMenu = new JMenuItem("Connect");
        fileMenu.add(connectMenu);
        connectMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK));

        JMenuItem disconnectMenu = new JMenuItem("Disconnect");
        fileMenu.add(disconnectMenu);
        disconnectMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK));

        fileMenu.addSeparator();

        fileMenu.addSeparator();

        JMenuItem exitMenu = new JMenuItem("Exit");
        fileMenu.add(exitMenu);

        JMenu helpMenu = new JMenu("Help");

        JMenuItem aboutMenu = new JMenuItem("About");
        helpMenu.add(aboutMenu);

        menuBar.add(fileMenu);
        menuBar.add(helpMenu);

        connectMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ConnectAgentToDF();
            }
        });

        disconnectMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DisconnectAgentFromDF();
            }
        });

        exitMenu.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        aboutMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AboutGUIForm aboutForm = new AboutGUIForm("Manufacture Manager", GUIManufactureForm.this);
            }
        });

        this.setJMenuBar(menuBar);
    }
    /*=====================================*/

    /*Реализация взаимодействия с системой DF через GUIAgent*/
    private void ConnectAgentToDF() {
        if(!isConnected) {
            GuiEvent _guiEvent = new GuiEvent(this, 0);
            manufactureManager.postGuiEvent(_guiEvent);

            isConnected = true;
        }
        else {
            printLogMessage("You already registered in the system!");
        }
    }

    private void DisconnectAgentFromDF() {
        if(isConnected) {
            GuiEvent _guiEvent = new GuiEvent(this, 1);
            manufactureManager.postGuiEvent(_guiEvent);

            isConnected = false;
        }
        else {
            printLogMessage("You are not registering in the system!");
        }
    }
    /*======================================================*/

    /*Функция для вывода сообщений либо от GUIAgent, либо от самого класса, реализующий
    * графический интерфейс*/
    public void printLogMessage(String _message) {
        Date dataTime = new Date();
        SimpleDateFormat formatDataTime = new SimpleDateFormat("[HH:mm:ss]: ");

        logMessage.append(formatDataTime.format(dataTime) + _message + "\n");
        logMessage.setCaretPosition(logMessage.getDocument().getLength());
    }
    /*================================================================================*/

    public GUIManufactureForm(GUIManufacture _e) {
        super("Manufacture");

        manufactureManager = _e;

        setContentPane(rootPanel);
        CreateMenuBar();

        setResizable(false);
        setSize(280, 520);

        setLocationRelativeTo(null);

        this.setVisible(true);

        printLogMessage("Welcome to Manufacture Manager!");
    }
}
