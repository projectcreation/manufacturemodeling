package GUI.About.GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Klimov Denis
 * Date: 07.05.14
 * Time: 22:52
 * To change this template use File | Settings | File Templates.
 */
public class AboutGUIForm extends JDialog {

    private String aboutText = " agent was developed by JTeamGroup.";
    private JPanel rootPanel;
    private JButton OK;
    private JTextArea labelAbout;

    public AboutGUIForm(String nameAgent, JFrame parent) {
        super(parent, "About", true);

        setResizable(false);
        setContentPane(rootPanel);
        setSize(300, 150);
        setLocationRelativeTo(parent);

        labelAbout.setText(nameAgent + aboutText);
        labelAbout.setBackground(new Color(0, 0, 0, 0));
        labelAbout.setCursor(null);
        OK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        setVisible(true);
    }
}
