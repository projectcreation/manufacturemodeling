package GUI.Deals.Manager.GUI;

import Deals.Manager.GUIAgent;
import GUI.About.GUI.*;
import GUI.Setting.GUI.*;
import SourceClasses.*;
import jade.core.AID;
import jade.gui.GuiEvent;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.event.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/*Реализация класса, расширяещего возомжности стандартного класса AbstractTableModel*/
class MyTableModel extends AbstractTableModel {
    private String[] columnNames = {"Name"};

    private ArrayList<Department> arrayTableModel = new ArrayList<Department>();

    public MyTableModel(ArrayList<Department> departments) {
        this.arrayTableModel = departments;
    }

    public int getRowCount() {
        return arrayTableModel.size();
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        if(columnIndex!=0)
            return "";
        return arrayTableModel.get(rowIndex).getName();
    }

    public AID getAddressAt(int rowIndex) {
        return arrayTableModel.get(rowIndex).getAddress();
    }

    public String getColumnName(int columnIndex) {
        switch(columnIndex) {
            case 0:
                return "Name";
        }
        return "";
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public void insertData(Department _rowsName){
        if(!arrayTableModel.contains(_rowsName)) {
            arrayTableModel.add(_rowsName);
            fireTableDataChanged();
        }
    }

    public void removeRow(int rowIndex){
        arrayTableModel.remove(rowIndex);
        fireTableDataChanged();
    }

    public void updateTableData(ArrayList<Department> _newNamesRow) {
        for (Department a_newNamesRow : _newNamesRow) {
            if (!containsRow(arrayTableModel, a_newNamesRow)) {
                insertData(a_newNamesRow);
            }
        }

        for(int i = 0; i < arrayTableModel.size(); i++) {
            if(!containsRow(_newNamesRow, arrayTableModel.get(i))) {
                removeRow(i);
            }
        }
    }

    private boolean containsRow(ArrayList<Department> _departments, Department _object) {
        for (Department _department : _departments) {
            if (_department.getName().equals(_object.getName())) {
                return true;
            }
        }
        return false;
    }
}
/*==================================================================================*/

public class GUINewForm extends JFrame {

    private ArrayList<Department> department = new ArrayList<Department>();
    private GUIAgent dealManager;
    private MyTableModel tableModel;
    private String fileName;

    private JPanel rootPanel;
    private JPanel dealArea;
    private JPanel dealPanel;
    private JPanel manufacturesPanel;
    private JPanel messageArea;

    private JTable tableDB;
    private JTextArea messageTextArea;

    private JScrollPane tableScrollPane;
    private JScrollPane textAreaScrollPane;

    private JButton makeDealButton;
    private JButton getDataButton;
    private JButton analyzeButton;

    private JComboBox CaseBox;
    private JComboBox CPUBox;
    private JComboBox DisplayBox;
    private JComboBox RAMBox;
    private JComboBox CountBox;

    private JLabel Count;

    public Details gotDetails;
    public AID chosenDB;
    public boolean isConnected = false;

    /*Создание меню и полная его реализация*/
    private void CreateMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");

        JMenuItem connectMenu = new JMenuItem("Connect");
        fileMenu.add(connectMenu);
        connectMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK));

        JMenuItem disconnectMenu = new JMenuItem("Disconnect");
        fileMenu.add(disconnectMenu);
        disconnectMenu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_MASK));

        fileMenu.addSeparator();

        JMenuItem settingsMenu = new JMenuItem("Settings");
        fileMenu.add(settingsMenu);

        fileMenu.addSeparator();

        JMenuItem exitMenu = new JMenuItem("Exit");
        fileMenu.add(exitMenu);

        JMenu helpMenu = new JMenu("Help");

        JMenuItem aboutMenu = new JMenuItem("About");
        helpMenu.add(aboutMenu);

        menuBar.add(fileMenu);
        menuBar.add(helpMenu);

        connectMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ConnectAgentToDF();
            }
        });

        disconnectMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DisconnectAgentFromDF();
            }
        });

        settingsMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DBGUIForm settingForm = new DBGUIForm(GUINewForm.this, fileName);
            }
        });

        exitMenu.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        aboutMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AboutGUIForm aboutForm = new AboutGUIForm("Deals Manager", GUINewForm.this);
            }
        });

        this.setJMenuBar(menuBar);
    }
    /*=====================================*/

    /*Инициализация модели данных jTable*/
    private void SetTablesContent() {

        MyTableModel _myTableModel = new MyTableModel(department);
        tableModel = _myTableModel;
        tableDB.setModel(_myTableModel);
        tableDB.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
            }

            public void mousePressed(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    chosenDB = tableModel.getAddressAt(tableDB.getSelectedRow());
                }
            }

            public void mouseReleased(MouseEvent e) {
            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseExited(MouseEvent e) {
            }
        });
    }
    /*==================================*/

    /*Задание действий по событиям нажатия на jButton*/
    private void SetButtonAction() {
        analyzeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                gotDetails = new Details(CaseBox.getSelectedItem().toString(), CPUBox.getSelectedItem().toString(),
                        RAMBox.getSelectedItem().toString(), DisplayBox.getSelectedItem().toString(),
                        Integer.parseInt(CountBox.getSelectedItem().toString()));

                GuiEvent _guiEvent = new GuiEvent(this, 4);
                dealManager.postGuiEvent(_guiEvent);
            }
        });
        makeDealButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                gotDetails = new Details(CaseBox.getSelectedItem().toString(), CPUBox.getSelectedItem().toString(),
                        RAMBox.getSelectedItem().toString(), DisplayBox.getSelectedItem().toString(),
                        Integer.parseInt(CountBox.getSelectedItem().toString()));

                GuiEvent _guiEvent = new GuiEvent(this, 5);
                dealManager.postGuiEvent(_guiEvent);
            }
        });
        getDataButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                GuiEvent _guiEvent = new GuiEvent(this, 3);
                dealManager.postGuiEvent(_guiEvent);
            }
        });
    }
    /*===============================================*/

    /*Функция, служащая для обновления содержимого jTable*/
    public void UpdateTablesRow(ArrayList<Department> _rowsName) {
        tableModel.updateTableData(_rowsName);
    }
    /*===================================================*/

    /*Заолнение jComboBox*/
    public void setComboInformation(ArrayList<ManufactureDetails> _detailsList) {
        DefaultComboBoxModel CaseModel = (DefaultComboBoxModel)CaseBox.getModel();
        DefaultComboBoxModel CPUModel = (DefaultComboBoxModel)CPUBox.getModel();
        DefaultComboBoxModel DisplayModel = (DefaultComboBoxModel)DisplayBox.getModel();
        DefaultComboBoxModel RAMModel = (DefaultComboBoxModel)RAMBox.getModel();
        DefaultComboBoxModel CountModel = (DefaultComboBoxModel)CountBox.getModel();

        CaseModel.removeAllElements();
        CPUModel.removeAllElements();
        DisplayModel.removeAllElements();
        RAMModel.removeAllElements();
        CountModel.removeAllElements();

        CaseModel.addElement("-");
        CPUModel.addElement("-");
        DisplayModel.addElement("-");
        RAMModel.addElement("-");

        for (ManufactureDetails a_detailsList : _detailsList) {
            if (a_detailsList.getType().equals("Case")) {
                CaseModel.addElement(a_detailsList.getName());
            }
            if (a_detailsList.getType().equals("CPU")) {
                CPUModel.addElement(a_detailsList.getName());
            }
            if (a_detailsList.getType().equals("Display")) {
                DisplayModel.addElement(a_detailsList.getName());
            }
            if (a_detailsList.getType().equals("RAM")) {
                RAMModel.addElement(a_detailsList.getName());
            }
        }

        CountModel.addElement("-");

        for(int i = 0; i < 5; i++) {
            CountModel.addElement((i+1)*10);
        }
    }
    /*===================*/

    /*Реализация взаимодействия с системой DF через GUIAgent*/
    private void ConnectAgentToDF() {
        if(!isConnected) {
            GuiEvent _guiEvent = new GuiEvent(this, 0);
            dealManager.postGuiEvent(_guiEvent);

            isConnected = true;
        }
        else {
            printLogMessage("You already registered in the system!");
        }
    }

    private void DisconnectAgentFromDF() {
        if(isConnected) {
            GuiEvent _guiEvent = new GuiEvent(this, 1);
            dealManager.postGuiEvent(_guiEvent);

            isConnected = false;
        }
        else {
            printLogMessage("You are not registering in the system!");
        }
    }
    /*======================================================*/

    /*Функция для вывода сообщений либо от GUIAgent, либо от самого класса, реализующий
    * графический интерфейс*/
    public void printLogMessage(String _message) {
        Date dataTime = new Date();
        SimpleDateFormat formatDataTime = new SimpleDateFormat("[HH:mm:ss]: ");

        messageTextArea.append(formatDataTime.format(dataTime) + _message + "\n");
        messageTextArea.setCaretPosition(messageTextArea.getDocument().getLength());
    }

    public void clearLogMessage() {
        messageTextArea.setText("");
    }
    /*================================================================================*/

    public GUINewForm(GUIAgent _e, String _fileName) {
        super("Deals Manager");

        fileName = _fileName;
        dealManager = _e;

        setContentPane(rootPanel);
        setResizable(false);

        setSize(455, 655);
        setLocationRelativeTo(null);
        SetButtonAction();
        SetTablesContent();
        CreateMenuBar();

        setVisible(true);

        printLogMessage("Welcome to Deals Manager!");
    }
}
