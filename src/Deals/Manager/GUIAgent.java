package Deals.Manager;

import GUI.Deals.Manager.GUI.GUINewForm;
import SourceClasses.DealsFeatures;
import SourceClasses.*;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.gui.GuiAgent;
import jade.gui.GuiEvent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;

import javax.swing.*;
import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class GUIAgent extends GuiAgent {

    private String fileName = "C://MyApplication/confdm";
    private File fileSettings = new File(fileName);
    private boolean isSetting;
    private GUINewForm _agentForm;

    private String userName;
    private String userPassword;
    private String dbURL;
    private String dbName;

    private Timer dynamicUpdate;

    public ArrayList<ManufactureDetails> _details = new ArrayList<ManufactureDetails>();

    /*Подключение к БД и получение необходимых данных*/
    private boolean DriverRegistration() {
        try {

            Class.forName("org.postgresql.Driver");

        } catch (ClassNotFoundException e) {

            System.out.println("Where is your PostgreSQL JDBC Driver? "
                    + "Include in your library path!");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private Connection ConnectionToDatabase() {
        Connection connection;

        try {
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://" + dbURL + "/" + dbName, userName,
                    userPassword);
        } catch (SQLException e) {
            _agentForm.printLogMessage("Connection Failed! Data was not loaded.");
            e.printStackTrace();
            return null;
        }

        return connection;
    }

    private void getDataFromDataBase() throws SQLException {
        _details.clear();

        Connection _connection = null;
        Statement _statement = null;

        if(DriverRegistration()) {
            try {
                _connection = ConnectionToDatabase();
                _statement = _connection.createStatement();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            ResultSet _resultSet = null;
            if (_statement != null) {
                _resultSet = _statement.executeQuery("SELECT * FROM \"Details\"");
            }

            if (_resultSet != null) {
                while (_resultSet.next()) {
                    _details.add(new ManufactureDetails(_resultSet.getString(3), _resultSet.getString(2)));
                }
            }

            try {
                _statement.close();
            } catch (SQLException _e) { _e.printStackTrace(); }
            try {
                _connection.close();
            } catch (SQLException _e) { _e.printStackTrace(); }
        }
    }
    /*===============================================*/

    /*Получение данных из БД и установка их в соответствующие jComboBox*/
    private void setDataInGUIBoxes() throws SQLException {
        getDataFromDataBase();
        _agentForm.setComboInformation(_details);
    }
    /*=================================================================*/

    /*Реализация поиска определенных агентов в DF*/
    public void findSomeAgents(String _groupsName) {
        DFAgentDescription _agentDescription = new DFAgentDescription();
        ServiceDescription _serviceDescription  = new ServiceDescription();
        _serviceDescription.setType(_groupsName);
        _agentDescription.addServices(_serviceDescription);

        DFAgentDescription[] result = new DFAgentDescription[0];
        try {
            result = DFService.search(this, _agentDescription);
        } catch (FIPAException ignored) {}

        ArrayList<Department> _names = new ArrayList<Department>();

        for (DFAgentDescription aResult : result) {
            _names.add(new Department(aResult.getName().getLocalName(), aResult.getName()));
        }

        _agentForm.UpdateTablesRow(_names);
    }
    /*===========================================*/

    /*Проверка наличия и получение настроек подключения к БД*/
    private void checkConnectionSettings() {
        if(!fileSettings.exists()) {
            isSetting = false;
            _agentForm.printLogMessage("You should to set settings!");
        }
        else {
            try {
                getConnectionSettings();
            } catch (FileNotFoundException ignored) {}
            isSetting = true;
            _agentForm.printLogMessage("You got settings!");
        }
    }

    private void getConnectionSettings() throws FileNotFoundException {
        ArrayList<String> _settings = new ArrayList<String>();
        if(fileSettings.exists()) {
            try {
                BufferedReader in = new BufferedReader(new FileReader(fileSettings.getAbsoluteFile()));
                try {
                    String line;
                    while ((line = in.readLine()) != null) {
                        _settings.add(line);
                    }
                } catch (IOException ignored) {}
                finally {
                    in.close();
                }
            } catch(IOException e) {
                throw new RuntimeException(e);
            }

            for(int i = 0; i < 4; i++) {
                switch(i) {
                    case 0:
                        userName = _settings.get(i);
                        break;
                    case 1:
                        userPassword = _settings.get(i);
                        break;
                    case 2:
                        dbURL = _settings.get(i);
                        break;
                    case 3:
                        dbName = _settings.get(i);
                        break;
                }
            }

            isSetting = true;
        }
        else {
            _agentForm.printLogMessage("You should to set settings!");
        }
    }
    /*======================================================*/

    public void setup() {
        try {
            UIManager.setLookAndFeel("com.alee.laf.WebLookAndFeel");
        } catch (ClassNotFoundException d) { System.out.println("ClassNotFoundException " + d); }
        catch (InstantiationException d) { System.out.println("InstantiationException " + d); }
        catch (IllegalAccessException d) { System.out.println("IllegalAccessException " + d); }
        catch (UnsupportedLookAndFeelException d) { System.out.println("UnsupportedLookAndFeelException " + d); }

        _agentForm = new GUINewForm(this, fileName);

        checkConnectionSettings();
        agentCommunication();
    }

    protected void onGuiEvent(GuiEvent guiEvent) {
        if(guiEvent.getType() == 0) {
            try {
                getConnectionSettings();
            } catch (FileNotFoundException ignored) {}
            if(isSetting) {
                DFAgentDescription _connection = new DFAgentDescription();
                _connection.setName(getAID());

                ServiceDescription _connectionSD  = new ServiceDescription();
                _connectionSD.setType("GUI");
                _connectionSD.setName(getAID().toString());
                _connection.addServices(_connectionSD);

                try {
                    DFService.register(this, _connection);
                } catch (FIPAException ignored) {}

                try {
                    getConnectionSettings();
                } catch (FileNotFoundException ignored) {}

                dynamicUpdate = new Timer();
                dynamicUpdate.schedule(new TimerTask() {
                    public void run() {
                        findSomeAgents("DB");
                    }
                }, 1000, 500);

                _agentForm.printLogMessage("You registered in the system.");
            } else {
                _agentForm.isConnected = false;
                _agentForm.printLogMessage("Settings is empty!");
            }
        }
        if(guiEvent.getType() == 1) {
            try {
                DFService.deregister(this);
            }
            catch (Exception ignored) {}

            dynamicUpdate.cancel();

            _agentForm.printLogMessage("You unregistered from the system.");
        }
        if(guiEvent.getType() == 3) {
            try {
                setDataInGUIBoxes();
            } catch (SQLException e) { e.printStackTrace(); }
        }
        if(guiEvent.getType() == 4) {
            ACLMessage msg  = new ACLMessage(ACLMessage.REQUEST);
            msg.addReceiver(_agentForm.chosenDB);
            try {
                msg.setContentObject(_agentForm.gotDetails);
            } catch (IOException ignored) {}
            msg.addUserDefinedParameter("ANLZ", "true");
            send(msg);
        }
        if(guiEvent.getType() == 5) {
            ACLMessage msg  = new ACLMessage(ACLMessage.REQUEST);
            msg.addReceiver(_agentForm.chosenDB);
            try {
                msg.setContentObject(_agentForm.gotDetails);
            } catch (IOException ignored) {}
            msg.addUserDefinedParameter("MD", "true");
            send(msg);
        }
    }

    private void agentCommunication() {
        addBehaviour(new CyclicBehaviour(this) {
            public void action() {
                ACLMessage msg = receive();
                if (msg != null) {
                    if(msg.getUserDefinedParameter("ACKAN")!=null) {
                        System.out.println(getLocalName() + ": Получен ответ!");
                        _agentForm.clearLogMessage();
                        ArrayList<DealsFeatures> Test = null;
                        try {
                            Test = (ArrayList<DealsFeatures>)msg.getContentObject();
                        } catch (UnreadableException e) {}

                        if (Test != null) {
                            String _informationMessage = "Information about deal:\n";
                            _informationMessage+= "=====================\n";
                            for(int i=0;i<Test.size(); i++) {
                                if(!Test.get(i).getOrder()) {
                                    _informationMessage+= "Devices Detail: " + Test.get(i).getDetailsName() + "\n" +
                                            "Price: " + Test.get(i).getPrice() + "\n" +
                                            "Making Time: " + Test.get(i).getTime() + "\n";
                                }
                                else {
                                    _informationMessage+= "Devices Detail: " + Test.get(i).getDetailsName() + "\n" +
                                            "This detail will be collect from other details!" + "\n" +
                                            "Count of collecting devices: " + Test.get(i).getCount() + "\n" +
                                            "Price on details in storage: " + Test.get(i).getPrice() + "\n" +
                                            "Price on details that will be collect: " + Test.get(i).getMorePrice() + "\n" +
                                            "Making time on details in storage: " + Test.get(i).getTime() + "\n" +
                                            "Making time on details that will be collect: " + Test.get(i).getMoreTime() + "\n";
                                }
                                _informationMessage+= "=====================\n";
                            }

                            int allPrice = 0,
                                allTime = 0;
                            for(int i = 0; i < Test.size(); i++) {
                                allPrice+= Test.get(i).getPrice() + Test.get(i).getMorePrice();
                                allTime+= Test.get(i).getTime() + Test.get(i).getMoreTime();
                            }

                            _informationMessage+= "Price on device: " + allPrice + "\n" +
                                    "Time that need to make device: " + allTime + "\n" +
                                    "=====================\n";

                            _agentForm.printLogMessage(_informationMessage);
                        }
                        if (Test != null) {
                            Test.clear();
                        }
                    }
                    if(msg.getUserDefinedParameter("ACKMD")!=null) {
                        _agentForm.printLogMessage("Answer from " + msg.getSender().getLocalName() + ": " + msg.getContent());
                    }
                    if(msg.getUserDefinedParameter("ACKMA")!=null) {
                       _agentForm.printLogMessage("Answer from " + msg.getSender().getLocalName() + ": " + msg.getContent());
                    }
                }
                block();
            }
        });
    }
}
