package Manufactures;

import Deals.Manager.GUIAgent;
import GUI.Manufctures.Manager.GUI.GUIManufactureForm;
import SourceClasses.Details;
import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.gui.GuiEvent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;

import javax.swing.*;

public class GUIManufacture extends GUIAgent {
    GUIManufactureForm _agentForm;
    Details _gottenDetails;
    AID _addressGUIAgent;
    public void setup() {

        try {
            UIManager.setLookAndFeel("com.alee.laf.WebLookAndFeel");
        } catch (ClassNotFoundException ignored) {}
        catch (InstantiationException ignored) {}
        catch (IllegalAccessException ignored) {}
        catch (UnsupportedLookAndFeelException ignored) {}

        GUIManufactureForm Form = new GUIManufactureForm(this);
        _agentForm = Form;

         agentCommunication();
    }

    protected void onGuiEvent(GuiEvent guiEvent) {
        if(guiEvent.getType() == 0) {

            DFAgentDescription _connection = new DFAgentDescription();
            _connection.setName(getAID());

            ServiceDescription _connectionSD  = new ServiceDescription();
            _connectionSD.setType("MD");
            _connectionSD.setName(getAID().toString());
            _connection.addServices(_connectionSD);

            try {
                DFService.register(this, _connection);
            } catch (FIPAException ignored) {}
        }
        if(guiEvent.getType() == 1) {
            try {
                DFService.deregister(this);
            }
            catch (Exception ignored) {}

            _agentForm.printLogMessage("You unregistered from the system.");
        }
    }

    private void agentCommunication() {
        addBehaviour(new CyclicBehaviour(this) {
            public void action() {
                ACLMessage msg = receive();
                if (msg != null) {
                    if(msg.getUserDefinedParameter("MAD")!=null) {
                        System.out.println(getLocalName() + ": Получен запрос на товар!");
                        try {
                            _gottenDetails = (Details)msg.getContentObject();
                        } catch (UnreadableException ignored) {}
                        if (_gottenDetails != null) {

                            String _informationMessage = "Request for make from " + msg.getSender().getLocalName() + ":\n";
                            _informationMessage+= "=====================\n" +
                                    "Case: " + _gottenDetails.getCase() + "\n" +
                                    "Display: " + _gottenDetails.getDisplay() + "\n" +
                                    "CPU: " + _gottenDetails.getCPU() + "\n" +
                                    "RAM: " + _gottenDetails.getRAM() + "\n" +
                                    "Count: " + _gottenDetails.getCount() + "\n" +
                                    "=====================\n";
                            _agentForm.printLogMessage(_informationMessage);
                        }
                    }
                    if(msg.getUserDefinedParameter("MADADD")!=null) {
                        System.out.println(getLocalName() + ": Получен запрос на товар!");
                        try {
                            _addressGUIAgent = (AID)msg.getContentObject();
                        } catch (UnreadableException ignored) {}
                        if (_addressGUIAgent != null) {

                            ACLMessage reqMsg = new ACLMessage(ACLMessage.REQUEST);
                            reqMsg.addUserDefinedParameter("ACKMA", "true");
                            reqMsg.addReceiver(_addressGUIAgent);
                            reqMsg.setContent("All details were collect. You'll can take ready devices in our department.");
                            send(reqMsg);
                        }
                    }
                }
                block();
            }
        });
    }
}
