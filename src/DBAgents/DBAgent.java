package DBAgents;

import GUI.Storage.Manager.GUI.*;
import SourceClasses.DealsFeatures;
import SourceClasses.Details;
import SourceClasses.ListDetails;
import SourceClasses.ListWorking;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.domain.FIPAException;
import jade.gui.GuiAgent;
import jade.gui.GuiEvent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;

import javax.swing.*;
import java.io.*;
import java.sql.*;
import java.util.ArrayList;

public class DBAgent extends GuiAgent {
    private String fileName = "C://MyApplication/confsm";
    private File fileSettings = new File(fileName);

    private String userName;
    private String userPassword;
    private String dbURL;
    private String dbName;

    private GUIStorageForm _agentForm;

    private ArrayList<ListDetails> _detailsList = new ArrayList<ListDetails>();
    private ArrayList<ListWorking> _workingList = new ArrayList<ListWorking>();
    private ArrayList<DealsFeatures> _dealsFeatureList = new ArrayList<DealsFeatures>();

    private int countMoney;
    private int countTime;

    private Details _gottenDetails = null;
    private boolean isSetting = false;
    private boolean isMakingDeal = false;

    /*Поиск в дереве по необходимым критериям*/
    public ListDetails getDetailByName(String _name) {
        for(int i = 0;i < _detailsList.size(); i++) {
            if(_detailsList.get(i).getName().equalsIgnoreCase(_name)) {
                return _detailsList.get(i);
            }
        }
        return null;
    }

    public String getWorkingById(int _id) {
        for (ListWorking a_workingList : _workingList) {
            if (a_workingList.getId() == _id) {
                return a_workingList.getName();
            }
        }
        return null;
    }
    /*=======================================*/

    /*Подключение к БД и получение необходимых данных*/
    private boolean DriverRegistration() {
        try {

            Class.forName("org.postgresql.Driver");

        } catch (ClassNotFoundException e) {

            System.out.println("Where is your PostgreSQL JDBC Driver? "
                    + "Include in your library path!");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private Connection ConnectionToDatabase() {
        Connection connection;

        try {
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://" + dbURL + "/" + dbName, userName,
                    userPassword);
        } catch (SQLException e) {
            _agentForm.printLogMessage("Connection Failed! Data was not loaded.");
            e.printStackTrace();
            return null;
        }

        return connection;
    }

    private void updateDataInDataBase() {
        Connection _connection = null;
        PreparedStatement _statement = null;

        if(DriverRegistration()) {
            try {
                _connection = ConnectionToDatabase();
                _statement = _connection.prepareStatement("UPDATE \"public\".\"DetailsTree\" SET \"CountDevice\" = ? WHERE \"pid\" != -1 AND \"id\" = ?");
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            for (ListDetails a_detailsList : _detailsList) {
                if (a_detailsList.getPid() != -1) {
                    try {
                        assert _statement != null;
                        _statement.setInt(1, a_detailsList.getCount());
                        _statement.setInt(2, a_detailsList.getId());

                        try {
                            _statement.executeUpdate();
                        } catch (SQLException ignored) {}
                    } catch (SQLException ignored) {}
                }
            }

            try {
                assert _statement != null;
                _statement.close();
            } catch (SQLException _e) { _e.printStackTrace(); }
            try {
                _connection.close();
            } catch (SQLException _e) { _e.printStackTrace(); }
        }
    }

    private void getDataFromDataBase() throws SQLException {

        Connection _connection = null;
        Statement _statement = null;

        if(DriverRegistration()) {
            try {
                _connection = ConnectionToDatabase();
                _statement = _connection.createStatement();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            ResultSet _resultSet = null;
            if (_statement != null) {
                _resultSet = _statement.executeQuery("SELECT * FROM \"DetailsTree\"");
            }

            if (_resultSet != null) {
                while (_resultSet.next()) {
                    _detailsList.add(new ListDetails(_resultSet.getInt("id"), _resultSet.getInt("pid"),
                            _resultSet.getString("namedevice"), _resultSet.getInt("countdevice"),
                            _resultSet.getInt("price"), _resultSet.getInt("needtodeal"),
                            _resultSet.getInt("mprocess")));
                }
            }

            _resultSet = null;
            if (_statement != null) {
                _resultSet = _statement.executeQuery("SELECT * FROM \"MakingProcesses\"");
            }

            if (_resultSet != null) {
                while (_resultSet.next()) {
                    _workingList.add(new ListWorking(_resultSet.getInt("id"), _resultSet.getString("nameprocess"),
                            _resultSet.getInt("time")));
                }
            }

            try {
                _statement.close();
            } catch (SQLException _e) { _e.printStackTrace(); }
            try {
                _connection.close();
            } catch (SQLException _e) { _e.printStackTrace(); }
        }
    }
    /*===============================================*/

    /*Проверка наличия и получение настроек подключения к БД*/
    private void checkConnectionSettings() {
        if(!fileSettings.exists()) {
            isSetting = false;
            _agentForm.printLogMessage("You should to set settings!");
        }
        else {
            try {
                getConnectionSettings();
            } catch (FileNotFoundException ignored) {}
            isSetting = true;
            _agentForm.printLogMessage("You got settings!");
        }
    }

    private void getConnectionSettings() throws FileNotFoundException {
        ArrayList<String> _settings = new ArrayList<String>();
        if(fileSettings.exists()) {
            try {
                BufferedReader in = new BufferedReader(new FileReader(fileSettings.getAbsoluteFile()));
                try {
                    String line;
                    while ((line = in.readLine()) != null) {
                        _settings.add(line);
                    }
                } catch (IOException ignored) {}
                finally {
                    in.close();
                }
            } catch(IOException e) {
                throw new RuntimeException(e);
            }

            for(int i = 0; i < 4; i++) {
                switch(i) {
                    case 0:
                        userName = _settings.get(i);
                        break;
                    case 1:
                        userPassword = _settings.get(i);
                        break;
                    case 2:
                        dbURL = _settings.get(i);
                        break;
                    case 3:
                        dbName = _settings.get(i);
                        break;
                }
            }

            isSetting = true;
        }
        else {
            _agentForm.printLogMessage("You should to set settings!");
        }
    }
    /*======================================================*/

    /*Функции, реализующие механизм поиска и анализа процесса сборки готового изделия*/
    private void setNullCountData() {
        countMoney = 0;
        countTime = 0;
    }

    private void countLowLevelDetails(ArrayList<String> _list, int _countUpLevelDetail) {
        for (String a_list : _list) {
            ListDetails gottenDetail = findDetail(_detailsList, a_list);

            if (gottenDetail.getCount() >= (_countUpLevelDetail * gottenDetail.getNeedToDeal())) {
                countMoney += gottenDetail.getPrice() * _countUpLevelDetail * gottenDetail.getNeedToDeal();
                for (ListWorking a_workingList : _workingList) {
                    if (a_workingList.getId() == gottenDetail.getMakingProcess()) {
                        countTime += a_workingList.getTime() * _countUpLevelDetail * gottenDetail.getNeedToDeal();
                    }
                }
                if(isMakingDeal) {
                    for (ListDetails a_detailsList : _detailsList) {
                        if (a_detailsList.getName().equals(a_list)) {
                            a_detailsList.setCount(a_detailsList.getCount() - _countUpLevelDetail * gottenDetail.getNeedToDeal());
                        }
                    }
                }
            } else {
                countMoney += gottenDetail.getPrice() * gottenDetail.getCount();
                for (ListWorking a_workingList : _workingList) {
                    if (a_workingList.getId() == gottenDetail.getMakingProcess()) {
                        countTime += a_workingList.getTime() * _countUpLevelDetail * gottenDetail.getNeedToDeal();
                    }
                }
                if(isMakingDeal) {
                    for (ListDetails a_detailsList : _detailsList) {
                        if (a_detailsList.getName().equals(a_list)) {
                            a_detailsList.setCount(0);
                        }
                    }
                }
                countLowLevelDetails(findDetails(_detailsList, gottenDetail.getId()),
                        _countUpLevelDetail * gottenDetail.getNeedToDeal() - gottenDetail.getCount());

            }
        }
    }

    private void getStatusProduct(ArrayList<String> _list) {
        for (String a_list : _list) {
            ListDetails gottenDetail = findDetail(_detailsList, a_list);

            if (gottenDetail.getCount() >= _gottenDetails.getCount()) {
                countMoney += gottenDetail.getPrice() * _gottenDetails.getCount();
                for (ListWorking a_workingList : _workingList) {
                    if (a_workingList.getId() == gottenDetail.getMakingProcess()) {
                        countTime += a_workingList.getTime() * _gottenDetails.getCount();
                    }
                }

                if(isMakingDeal) {
                    for (ListDetails a_detailsList : _detailsList) {
                        if (a_detailsList.getName().equals(a_list)) {
                            a_detailsList.setCount(a_detailsList.getCount() - _gottenDetails.getCount());
                        }
                    }
                } else {
                    _dealsFeatureList.add(new DealsFeatures(a_list, false, gottenDetail.getCount(), countMoney, 0, countTime, 0));
                }

                setNullCountData();
            } else {
                countMoney += gottenDetail.getPrice() * gottenDetail.getCount();
                int _countMoney = countMoney,
                        _countTime = 0;
                for (ListWorking a_workingList : _workingList) {
                    if (a_workingList.getId() == gottenDetail.getMakingProcess()) {
                        countTime += a_workingList.getTime() * gottenDetail.getCount();
                    }
                }
                _countTime = countTime;
                ArrayList<String> _lowLevelDetails = findDetails(_detailsList, gottenDetail.getId());
                countLowLevelDetails(_lowLevelDetails, _gottenDetails.getCount() - gottenDetail.getCount());

                if(isMakingDeal) {
                    for (ListDetails a_detailsList : _detailsList) {
                        if (a_detailsList.getName().equals(a_list)) {
                            a_detailsList.setCount(0);
                        }
                    }
                } else {
                    _dealsFeatureList.add(new DealsFeatures(a_list, true, _gottenDetails.getCount() - gottenDetail.getCount(),
                            _countMoney, countMoney - _countMoney, _countTime, countTime - _countTime));
                }
            }
            setNullCountData();
        }
        if(isMakingDeal) {
            isMakingDeal = false;

            updateDataInDataBase();
        }
    }

    private ListDetails findDetail(ArrayList<ListDetails> _list, String _detailsName) {
        for (ListDetails a_list : _list) {
            if (a_list.getName().equals(_detailsName)) {
                return a_list;
            }
        }
        return null;
    }

    private ArrayList<String> findDetails(ArrayList<ListDetails> _list, int _id) {
        ArrayList<String> _gottenList = new ArrayList<String>();

        for (ListDetails a_list : _list) {
            if (a_list.getPid() == _id) {
                _gottenList.add(a_list.getName());
            }
        }

        return _gottenList;
    }

    private void findAllDetails(ArrayList<ListDetails> _list, int _id) {
        for (int i = 0; i < _list.size(); i++) {
            if(_list.get(i).getPid() == _id) {
                findAllDetails(_list, _list.get(i).getId());
            }
        }
    }
    /*===============================================================================*/

    public void setup() {

        try {
            UIManager.setLookAndFeel ("com.alee.laf.WebLookAndFeel");
        } catch (ClassNotFoundException ignored) {}
        catch (InstantiationException ignored) {}
        catch (IllegalAccessException ignored) {}
        catch (UnsupportedLookAndFeelException ignored) {}

        GUIStorageForm _form = new GUIStorageForm(this, fileName);
        _agentForm = _form;

        agentCommunication();
        checkConnectionSettings();

        try {
            getDataFromDataBase();
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    protected void onGuiEvent(GuiEvent guiEvent) {
        if(guiEvent.getType() == 0) {
            try {
                getConnectionSettings();
            } catch (FileNotFoundException ignored) {}
            if(isSetting) {
                DFAgentDescription _connection = new DFAgentDescription();
                _connection.setName(getAID());

                ServiceDescription _connectionSD  = new ServiceDescription();
                _connectionSD.setType("DB");
                _connectionSD.setName(getAID().toString());
                _connection.addServices(_connectionSD);

                try {
                    DFService.register(this, _connection);
                } catch (FIPAException ignored) {}

                try {
                    getConnectionSettings();
                } catch (FileNotFoundException ignored) {}

                _agentForm.createTreeModel(_detailsList);
                _agentForm.printLogMessage("You registered in the system.");
            } else {
                _agentForm.isConnected = false;
            }
        }
        if(guiEvent.getType() == 1) {
            try {
                DFService.deregister(this);
            }
            catch (Exception ignored) {}

            _agentForm.printLogMessage("You unregistered from the system.");
        }
    }

    private void agentCommunication() {
        addBehaviour(new CyclicBehaviour(this) {
            public void action() {
                ACLMessage msg = receive();
                if (msg != null) {
                    if(msg.getUserDefinedParameter("ANLZ")!=null) {

                        try {
                           _gottenDetails = (Details)msg.getContentObject();
                        } catch (UnreadableException ignored) {}
                        if (_gottenDetails != null) {

                            String _informationMessage = "Request for analysis from " + msg.getSender().getLocalName() + ":\n";
                            _informationMessage+= "=====================\n" +
                                    "Case: " + _gottenDetails.getCase() + "\n" +
                                    "Display: " + _gottenDetails.getDisplay() + "\n" +
                                    "CPU: " + _gottenDetails.getCPU() + "\n" +
                                    "RAM: " + _gottenDetails.getRAM() + "\n" +
                                    "Count: " + _gottenDetails.getCount() + "\n" +
                                    "=====================\n";
                            _agentForm.printLogMessage(_informationMessage);

                            setNullCountData();
                            getStatusProduct(_gottenDetails.getDevicesList());

                            ACLMessage replyMsg = msg.createReply();
                            replyMsg.addUserDefinedParameter("ACKAN", "true");
                            try {
                                replyMsg.setContentObject(_dealsFeatureList);
                            } catch (IOException ignored) {}
                            send(replyMsg);
                            _dealsFeatureList.clear();
                        }
                    }
                    if(msg.getUserDefinedParameter("MD")!=null) {
                        System.out.println(getLocalName() + ": Получен запрос на товар!");
                        try {
                            _gottenDetails = (Details)msg.getContentObject();
                        } catch (UnreadableException ignored) {}
                        if (_gottenDetails != null) {

                            String _informationMessage = "Request for make from " + msg.getSender().getLocalName() + ":\n";
                            _informationMessage+= "=====================\n" +
                                    "Case: " + _gottenDetails.getCase() + "\n" +
                                    "Display: " + _gottenDetails.getDisplay() + "\n" +
                                    "CPU: " + _gottenDetails.getCPU() + "\n" +
                                    "RAM: " + _gottenDetails.getRAM() + "\n" +
                                    "Count: " + _gottenDetails.getCount() + "\n" +
                                    "=====================\n";
                            _agentForm.printLogMessage(_informationMessage);

                            setNullCountData();

                            isMakingDeal = true;
                            getStatusProduct(_gottenDetails.getDevicesList());

                            ACLMessage replyMsg = msg.createReply();
                            replyMsg.addUserDefinedParameter("ACKMD", "true");
                            replyMsg.setContent("Details were collected and sent on manufacture.");
                            send(replyMsg);

                            DFAgentDescription _agentDescription = new DFAgentDescription();
                            ServiceDescription _serviceDescription  = new ServiceDescription();
                            _serviceDescription.setType("MD");
                            _agentDescription.addServices(_serviceDescription);

                            DFAgentDescription[] result = new DFAgentDescription[0];
                            try {
                                result = DFService.search(DBAgent.this, _agentDescription);
                            } catch (FIPAException ignored) {}


                            ACLMessage reqMsg = new ACLMessage(ACLMessage.REQUEST);
                            reqMsg.addUserDefinedParameter("MAD", "true");
                            reqMsg.addReceiver(result[0].getName());
                            try {
                                reqMsg.setContentObject(_gottenDetails);
                            } catch (IOException ignored) {}
                            send(reqMsg);

                            ACLMessage addReqMsg = new ACLMessage(ACLMessage.REQUEST);
                            addReqMsg.addUserDefinedParameter("MADADD", "true");
                            addReqMsg.addReceiver(result[0].getName());
                            try {
                                addReqMsg.setContentObject(msg.getSender());
                            } catch (IOException ignored) {}
                            send(addReqMsg);
                        }
                    }
                }
                block();
            }
        });
    }
}